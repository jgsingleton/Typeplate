const webpack = require('webpack');
const path = require('path');

module.exports = {
  devtool: 'source-map', //use source maps for debugging
  entry: [
    'babel-polyfill', //polyfill support for ES6
    'App.ts' //define the app entry point
  ],
  output: {
    filename: 'app.js', //compile into app.js...
    path: path.resolve('dist'), //...in the dist folder
    publicPath: '/dist' //expose it via /dist/app.js
  },

  //when importing a module, look for files with these extensions in these folders
  resolve: {
    modules: [path.resolve(__dirname, "src"),"node_modules"],
    extensions: ['*','.ts','.js','.css'] //resolve js, ts, and css files
  },
  module: {
    loaders: [
      { test: /\.ts?$/, loaders: ['babel-loader', 'ts-loader'] }, //compile .ts and .tsx files using babel
      { test: /\.css$/, loaders: ['style-loader', 'css-loader'] } //use "style.css" and compile into seperate file
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({ //minify output
      compress: { warnings: false }
    })
  ]
}