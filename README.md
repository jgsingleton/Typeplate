# Typeplate
A simple Typescript + Webpack template using:
* Typescript
* Webpack (Development + Production)
* Webpack Dev Server
* Babel

## Description
A simple project starter for Typescript + Webpack. Typescript and Webpack can be a pain to set up, therefore I created this template to quickly start doing Typescript web development.

## Installation
1. `npm install`
2. have fun!
