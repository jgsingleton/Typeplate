require("!style-loader!css-loader!./style.css");

class Startup {
    public static main(): HTMLElement {
        const text: HTMLElement = document.getElementById("message");
        text.innerText = "This works!";
        return text;
    }
}

Startup.main();